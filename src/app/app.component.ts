import { render, unmountComponentAtNode } from 'react-dom';
import { createElement } from 'react';

import { Component, OnInit } from '@angular/core';
import { Modal, ModalHeader, ModalBody, ModalFooter } from '@beyonder/react-components/build/dist/modal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  showModal = true;
  reactContainer: HTMLElement;

  modalTitle = 'My modal title';
  modalBody = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.' +
    'A ipsam magni maiores,\n\t  molestiae mollitia natus nobis, odio officia praesentium quibusdam ratione,' +
    '\n\t  reiciendis saepe ullam veritatis voluptatibus. Est eum ex odio.';
  modalFooter = 'Buttons or text content';

  inClassname = 'fadeIn';
  outClassname = 'fadeOut';

  ngOnInit(): void {
    this.reactContainer = document.getElementById('react-container');

    const props = { 
      inClassname: this.inClassname, 
      outClassname: this.outClassname, 
      onClose: this.removeReactModal() 
    };

    render(
      createElement(Modal, props, 
        createElement(ModalHeader, null, this.modalTitle),
        createElement(ModalBody, null, this.modalBody),
        createElement(ModalFooter, null, this.modalFooter)
      ),
      this.reactContainer
    );
  }

  removeAngularModal(message: string) {
    this.showModal = false;
  }

  removeReactModal() {
    return () => unmountComponentAtNode(this.reactContainer);
  }

}
