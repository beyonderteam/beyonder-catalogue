# beyonder-catalogue
Application qui constitue la documentation et le catalogue de l'ensemble des composants Beyonder.
Pour l'instant, aucun style n'a été défini mais j'imagine bien un écran avec un menu à gauche qui liste les composants et une barre de navigation horizontal qui comporte les implémentations : Angular | React | Vue.
Ainsi dès que l'on clique sur un composant ou bien une techno, le contenu de la page change pour montrer le composant monté + le code utilisé.

## Phase de développement
Une fois ce projet cloné, il faudra faire :
- `npm install`
- `npm link chemin_vers_beyonder_angular-components`
- `npm link chemin_vers_beyonder_react-components`

## Problème
Pour l'import du style des composants : côté Angular aucun problème, en revanche, côté React, il faut importer la feuille de style explicitement (je présume que c'est dù au principe d'isolation des styles de Angular).
Donc, à voir comment on peut gérer ça.